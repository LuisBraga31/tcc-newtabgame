import React from 'react';
import { useHistory } from 'react-router-dom';
import logo from '../../novologo.png';
import '../../css/TelaInicialAluno.css';

const TelaInicialAluno= () => {

  const history = useHistory();

  function entrar(){
    history.push('/nick');    
  }
  function comojogar(){
    history.push('/jogoAluno');    
  }
  function voltar(){
    history.push('/');    
  }

  return (
    <div className = "aluno-app">       
      <header className = "aluno-header">
      <img src={logo} className = "aluno-logo" alt="logo"/>
        
      <div className= "aluno-botoes">
        
        <h1>Seja Bem Vindo Aluno!</h1>
        <div className = "aluno-linha"> 
          <button onClick={entrar}> Entrar Partida </button>
        </div>
        <div className = "aluno-linha">
          <button onClick={comojogar}> Como Jogar ?</button>
        </div>
        <div>
          <button onClick={voltar} className= "aluno-voltar"> Voltar </button>
        </div>
      
      </div>
      </header>
    </div>
  );
}

export default TelaInicialAluno;