import React, { useContext } from 'react';
import StoreContext from '../../store/Context';
import { useHistory } from 'react-router-dom';
import logo from '../../novologo.png';
import '../../css/TelaInicialProfessor.css';

const TelaInicialProfessor = () => {
  
  const history = useHistory();
  const { token } = useContext(StoreContext);
  const { setToken } = useContext(StoreContext);
  
  function perfil(ev){
    ev.preventDefault();
    history.push('/perfil');    
  }
  function partida(ev){
    ev.preventDefault();
    history.push('/partida');    
  }
  function jogo(ev){
    ev.preventDefault();
    history.push('/jogo');    
  }
  function pergunta(ev){
    ev.preventDefault();
    history.push('/perguntas');    
  }
  function comojogar(ev){
    ev.preventDefault();
    history.push('/jogoProfessor');    
  }
  function sair(ev){
    ev.preventDefault();
    setToken(null);
    history.push('/');    
  }

  return (
    <div className="professor-app">       
      <header className="professor-header">
        <img src={logo} className="professor-logo" alt="logo"/>
        
        <div className= "professor-botoes">
  
          <h1>Seja Bem Vindo Professor {token.professor}!</h1>
          
          <div className = "professor-perfil">           
          <button onClick={perfil}> Meu Perfil </button>
          </div>     
          
          <div className = "professor-linha">  
          <button onClick={partida}> Nova Partida </button>
          <button onClick={jogo}> Criar Jogo </button>
          </div>
          
          <div className = "professor-linha">
          <button onClick={pergunta}> Criar Perguntas </button>  
          <button onClick={comojogar}> Como Jogar ?</button>
          </div>
          <div>
          <button className= "professor-sair" onClick={sair}> Sair </button>
          </div>
        
        </div>
        
      </header>
    </div>
  );
}

export default TelaInicialProfessor;