import React from 'react';
import  { useHistory } from 'react-router-dom';
import { DiProlog } from "react-icons/di";
import { DiGithubAlt } from "react-icons/di";
import logo from '../../novologo.png';
import '../../css/TelaInicial.css';

const TelaInicial = () => {
  
  const history = useHistory();
  
  function aluno(ev){
    ev.preventDefault();
    history.push('/Aluno');    
  }
  function professor(ev){
    ev.preventDefault();
    history.push('/login');    
  }

  return (
    <div className = "telainicial-app">       
      <header className="telainicial-header">
      <img src={logo} className="telainicial-logo" alt="logo"/>

        <h1>SEJA BEM VINDO AO TABGAME !</h1>

        <div>
        <button className="aluno-bt" onClick={aluno}> Aluno 
        <br/><DiGithubAlt fontSize="80px"/> 
        </button> 

        <button className="professor-bt" onClick={professor}> Professor
        <br/><DiProlog fontSize="80px"/> 
        </button> 
        </div>

      </header>
    </div>
  );
}

export default TelaInicial;