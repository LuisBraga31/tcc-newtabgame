import React from 'react';
import { useHistory } from 'react-router-dom';
import logo from '../../novologo.png';
import instrucao from '../../instruções-aluno.png';

import '../../css/ComoJogar.css';

const ComoJogarAluno = () => {

  const history = useHistory();

  return (
    <div className="comojogar-app">
      <header className="comojogar-header">
       
        <img src={logo} className="comojogar-logo" alt="logo" />
        <div className="explicacao"> 
        
        <h1> COMO JOGAR ? </h1>
        
        <img src={instrucao} className="instrucoes-professor" alt="inst-prof"/>

        </div>

        <button onClick={() => history.push('/Aluno')} className= "comojogar-voltar"> Voltar </button>

      </header>
    </div>
  );
}

export default ComoJogarAluno;