import React, { useEffect, useState, useContext } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import axios from 'axios';
import logo from '../../../novologo.png';
import teste from '../Components/teste.gif';
import './Sala.css';

import RecarregarPagina from '../Components/RecarregarPagina';
import ListaAlunos from '../Components/ListaAlunos';
import AlunoContext from '../../../store/AlunoContext';
import StoreContext from '../../../store/Context';

const Sala = () => {

  const { id } = useParams();
  
  const history = useHistory();
  const { token } = useContext(StoreContext);
  const { tokenAluno } = useContext(AlunoContext);
  const { setTokenAluno } = useContext(AlunoContext);

  const [partida, setPartida] = useState([]);
  const [aluno, setAluno] = useState([]);
  const [jogador, setJogador] = useState([]);

  const [questaoatual, setQuestaoAtual] = useState(0);
  const [pontos, setPontos] = useState(0);
  const [jogo, setJogo] = useState([]);
  const [jogopergunta, setJogopergunta] = useState([]);
  const [pergunta, setPergunta] = useState([]);

  const exibicao = useState([]);
  const [exibir, setExibir] = useState([]);

  useEffect(() => {

    axios.get(`https://localhost:44321/api/Partidas/${id}`) 
      .then((response) => {
        setPartida(response.data);
        console.log(response.data);
      });

    if(tokenAluno != null) {
    axios.get(`https://localhost:44321/api/Jogadores/${tokenAluno.idaluno}`) 
      .then((response) => {
        setJogador(response.data);
        console.log(response.data);
      }); }

    axios.get(`https://localhost:44321/api/Jogadores`) 
      .then((response) => {
        setAluno(response.data);
      });

    axios.get(`https://localhost:44321/api/Partidas/${id}`)
    .then((response) => {
      axios.get(`https://localhost:44321/api/Jogos/${response.data.jogoId}`)
      .then((response) => {
        setJogo(response.data);
        const busca = jogosperguntas => jogosperguntas.jogoId === response.data.id;
        axios.get('https://localhost:44321/api/JogosPerguntas')
        .then((response) => {
          setJogopergunta(response.data);
          const pesquisaPerguntaId = response.data.filter(busca).map(p => p.perguntaId);
          console.log(pesquisaPerguntaId);
          axios.get(`https://localhost:44321/api/Perguntas/${pesquisaPerguntaId[questaoatual]}`)
          .then((response) => {
            setPergunta(response.data);
          });
      });
    });
    });

  }, [id, questaoatual, tokenAluno]);

  function Iniciar(ev){
    ev.preventDefault();
      var atual = 0;

      axios.get(`https://localhost:44321/api/Partidas/${id}`)      
      .then((response) => {
        console.log(response.data.rodada);
        atual = response.data.rodada + 1;
        console.log(atual);
      });

      const atualizaState = {
        id: partida.id,
        codigo: partida.codigo,
        state: true,
        rodada: atual,
        qtdJogadores: partida.qtdJogadores,
        partidaInicio: partida.partidaInicio,
        partidaFim: partida.partidaFim,
        jogoId: partida.jogoId,
        professorId: partida.professorId
      };

      axios.put(`https://localhost:44321/api/Partidas/${id}`, atualizaState);
      alert('Partida Iniciada');
      return
  }

  function Finalizar(){
    history.push(`/resultadosPartida/${id}`);
  }
  
  function Sair(){
    
    setTokenAluno(null);

    axios.delete(`https://localhost:44321/api/Jogadores/${tokenAluno.idaluno}`)
      .then((response) => {
        history.push('/');
        window.location.reload(false);
      });
  }

  function verificando(ev) {
    ev.preventDefault();
      const busca = jogosperguntas => jogosperguntas.jogoId === jogo.id;
      const pesquisaPerguntaId = jogopergunta.filter(busca).map(p => p.perguntaId);  
    if(ev.target.name === pergunta.respCorreta){
      setPontos(pontos + 10);
    }
    console.log(pesquisaPerguntaId.length);
    var conta = pesquisaPerguntaId.length - 1;
    if(conta > questaoatual){
      axios.get(`https://localhost:44321/api/Perguntas/${pesquisaPerguntaId[questaoatual]}`)
      .then((response) => {
        setPergunta(response.data);
      });
      setQuestaoAtual(questaoatual + 1);
    
    } else {

      axios.get('https://localhost:44321/api/Resultados/')
      .then((response) => {
        
      for(var i = 0; i < response.data.length; i++){
          
        const a = tokenAluno.idaluno;
        const b = response.data[i].alunoId;
        
        if(a === b){ 
        const resultadofinal = {
          id: response.data[i].id,
          pontuacao: pontos,
          alunoId: response.data[i].alunoId
        };

        axios.put(`https://localhost:44321/api/Resultados/${response.data[i].id}`, resultadofinal)      
        .then((response) => {
          history.push(`/resultados`);
        });

        }
        
      }

      });

    }

    alert('Confirmar');
    return 
  }

  function exibirJogadores(ev){
    
    ev.preventDefault();
    
    for(var i = 0; i < aluno.length; i++){
        
      const a = partida.id;
      const b = aluno[i].partidaId;
        
      if(a === b){
        exibicao.push({id: aluno[i].id, nickname: aluno[i].nickname}); 
        }
    }

    exibicao.shift();
    exibicao.shift();
    setExibir(exibicao);
    console.log(exibicao);

    
  }

  return (
    
    <div>

    {token ? (

    <div  className = "professorjogo-app">       
      <header className = "professorjogo-header">
      <img src={logo} className = "professorjogo-logo" alt="logo"/>
      
      <div>

        <h1> Painel da Partida </h1>
        <label> Código: {partida.codigo} </label> <br/>

        <div className="professorjogo-painel">
          <button onClick={Iniciar}> Iniciar Partida </button>
          <button onClick={exibirJogadores}> Exibir Jogadores </button>
        </div>
      
        <div className = "professorJogo-tabela">
          <tr>
          <th width = "95">AlunoId</th>
          <th width = "400">NickName</th>
          </tr>
        {exibir.map((jogadores) => (<ListaAlunos jogadores={jogadores} /> ))}   
        </div>

      </div>
      
      <div>
        <button className= "professorjogo-finalizar" onClick ={Finalizar}> Finalizar Partida </button>
      </div>

      </header>
    </div>

    ) : (

    <div className = "alunojogo-app">       
      <header className = "alunojogo-header">

        <h1> TabGame - #{partida.codigo} </h1>
        <label>Jogador: {jogador.nickname}</label>

            <div className = "aplication">
      
            {!partida.state ? (

                <div  className="quiz-gif">
                
                    <img src={teste} alt="loading..." /> <br/>
                    <RecarregarPagina id ={id}/>
                 </div>

            ) : (

                <div className = "quiz-app">
                <div className="quiz-caixaresposta">
        
                <label>{pergunta.enunciado}</label>
                <div className="quiz-opcoes">
                    <button name = "A" onClick={verificando}>{pergunta.optionA}</button> <br/>
                    <button name = "B" onClick={verificando}>{pergunta.optionB}</button> <br/>
                    <button name = "C" onClick={verificando}>{pergunta.optionC}</button> <br/>
                    <button name = "D" onClick={verificando}>{pergunta.optionD}</button> <br/>
                </div>
        
                </div>
                </div>
            )}
            
            </div>
        
        <button className= "alunojogo-sair" onClick={Sair}> Sair da Partida </button>

      </header>
    </div>

    )}

    </div>
  );
}

export default Sala;