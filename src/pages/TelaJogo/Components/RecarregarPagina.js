import React, { useEffect, useState } from 'react';
import axios from 'axios';

const RecarregarPagina = ({ id }) => {
  
  const [partida, setPartida] = useState([]);

  useEffect(() => {
    axios.get(`https://localhost:44321/api/Partidas/${id}`) 
      .then((response) => {
        setPartida(response.data);
        console.log(response.data);
      });
  }, [id]);

  setInterval(
    function verificar(){
    if(partida.state === false) {
      
      setInterval(
        function refreshPage() {
          window.location.reload(false);
        }, 2000);
          
    } else {

    }
    }, 5000);

  return (
    <div>  
        
    </div>
  )
};

export default RecarregarPagina;