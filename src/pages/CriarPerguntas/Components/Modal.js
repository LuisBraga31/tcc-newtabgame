import React from 'react';
import './Moda.scss';

const Modal =({ onClose = () => {}, children}) => {
    
    return (

        <div className = "modal">      
            <div className = "modal-container">
            <button className = "modal-close" onClick={onClose}/>
            <div className = "modal-content">{children}</div>
            </div>
        </div>

    )
};

export default Modal;