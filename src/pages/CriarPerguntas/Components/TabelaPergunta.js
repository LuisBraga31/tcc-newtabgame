import React, { useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { BiTrash } from  'react-icons/bi'
import axios from 'axios';
import './TabelaPergunta.css';

import Pergunta from "./Pergunta";
import Modal from "./Modal";

 const TabelaPergunta = ({ perguntas }) => {
  
  const { id } = useParams();
  const [modal, setModal] = useState(false);
  const history = useHistory();
    
  function onClickDelete(ev){
      ev.preventDefault();

      axios.delete(`https://localhost:44321/api/Perguntas/${perguntas.id}`)
        .then((response) => {
          history.push('/');
        });
  }

  function editar(){
    setModal(true);
    history.push(`/edit/${perguntas.id}`);
  }

  function fechar(){
    setModal(false);
    history.push(`/perguntas`);
  }

  return (
    
    <div className = "tabeladeperguntas">

      <table>
        <tbody>
        <tr> 
          <td width = "20">{perguntas.id}</td>
          <td width = "185">{perguntas.tema}</td>
          <td width = "433">{perguntas.enunciado}</td>
          <td width = "70">
          <button onClick={editar}> Editar </button>
          {modal ? (<Modal onClose={fechar}> <Pergunta id={id ? Number.parseInt(id, 10) : null}/> </Modal>): null} </td>
          <td width = "50"> <button type = "button" onClick={onClickDelete}> <BiTrash/> </button> </td>
        </tr>

        </tbody>
      </table>

    </div>
  )
 };

export default TabelaPergunta;