import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import './Pergunta.css';

const valores = {
    tema: '',
    enunciado: '',
    dificuldade: 1,
    tempo: 30,
    optionA: '',
    optionB: '',
    optionC: '',
    optionD: '',
    respCorreta: 'A',
}

const Pergunta = ({ id }) => {

    const [valoresPergunta, setValoresPergunta] = useState(valores);
    const history = useHistory();
        
    useEffect(() => {
        if (id) {
            axios.get(`https://localhost:44321/api/Perguntas/${id}`)
            .then((response) => {
                setValoresPergunta(response.data);
            })
        }
    }, [id]);  

     function onChange(ev) {
        const { name, value } = ev.target;
        setValoresPergunta({ ...valoresPergunta, [name]: value });
    }

    function onSubmit(ev){
        ev.preventDefault();
        
        const postar = {
            tema: valoresPergunta.tema,
            enunciado: valoresPergunta.enunciado,
            dificuldade: parseInt(valoresPergunta.dificuldade),
            tempo: parseInt(valoresPergunta.tempo),
            optionA: valoresPergunta.optionA,
            optionB: valoresPergunta.optionB,
            optionC: valoresPergunta.optionC,
            optionD: valoresPergunta.optionD,
            respCorreta: valoresPergunta.respCorreta,
        }
        const editar = {
            id: valoresPergunta.id,
            tema: valoresPergunta.tema,
            enunciado: valoresPergunta.enunciado,
            dificuldade: parseInt(valoresPergunta.dificuldade),
            tempo: parseInt(valoresPergunta.tempo),
            optionA: valoresPergunta.optionA,
            optionB: valoresPergunta.optionB,
            optionC: valoresPergunta.optionC,
            optionD: valoresPergunta.optionD,
            respCorreta: valoresPergunta.respCorreta,
        }

        const method = id ? 'put' : 'post';
        const url = id
        ? `https://localhost:44321/api/Perguntas/${id}`
        : 'https://localhost:44321/api/Perguntas';
        const envio = id ? editar : postar;

        axios[method](url, envio)
        .then((response) => {
            history.push('/perguntas');
             window.location.reload(false);
        });
    }

    return (

        <div className="pergunta">
        <h1> Cadastrando Pergunta </h1>
        
        <form onSubmit={onSubmit}>  

            <input placeholder ="Tema" id = "tema" name = "tema" type = "text" onChange={onChange} value={valoresPergunta.tema} ></input>
            <input placeholder ="Enunciado" className="enunciado" id = "enunciado" name = "enunciado" type = "text" onChange={onChange} value={valoresPergunta.enunciado} ></input> 

            <label>Dificuldade: </label>
            <select id = "dificuldade" name = "dificuldade" onChange={onChange} value={valoresPergunta.dificuldade}>
                <option value = '1'> Fácil</option>
                <option value = '2'> Médio</option>
                <option value = '3'> Difícil</option>
            </select>

            <label> Tempo: </label>
            <select id ="tempo" name = "tempo" onChange={onChange} value={valoresPergunta.tempo}>
                <option value = '30' >30s</option>
                <option value = '45' >45s</option>
                <option value = '60' >60s</option>
            </select>

            <input placeholder ="Opção A" id= "optionA" name= "optionA" type = "text" onChange={onChange} value={valoresPergunta.optionA}></input>
            <input placeholder ="Opção B" id= "optionB" name= "optionB" type = "text" onChange={onChange} value={valoresPergunta.optionB}></input>
            <input placeholder ="Opção C" id= "optionC" name= "optionC" type = "text" onChange={onChange} value={valoresPergunta.optionC}></input>
            <input placeholder ="Opção D" id= "optionD" name = "optionD" type = "text" onChange={onChange} value={valoresPergunta.optionD}></input>
            
            <br/><label>Opção Correta: </label>
            <select id= "respCorreta" name = "respCorreta" onChange={onChange} value={valoresPergunta.respCorreta}> 
                <option value = "A">A</option>
                <option value = "B">B</option>
                <option value = "C">C</option>
                <option value = "D">D</option>
            </select>

            <br/><br/>
            <button type = "submit"> Salvar </button>
        </form> 
        
        </div>
    )
};

export default Pergunta;