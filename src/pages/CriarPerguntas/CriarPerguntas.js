import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import axios from 'axios';
import logo from '../../novologo.png';
import '../../css/CriarPerguntas.css';

import TabelaPergunta from './Components/TabelaPergunta';
import Pergunta from "./Components/Pergunta";
import Modal from "./Components/Modal";

const CriarPerguntas = () => {

  const { id } = useParams();
  const history = useHistory();
  const [modal, setModal] = useState(false);
  const [pergunta, setPerguntas] = useState([]);

  useEffect(() => {  
    const params = {};
    axios.get('https://localhost:44321/api/Perguntas', {params })
    .then((response) => {
      setPerguntas(response.data);
    });
  }, [id]);
  
  function voltar(){
    history.push('/Professor')
  }
  
  return (
    <div className="criarperguntas-app">
      <header className="criarperguntas-header">
      <img src={logo} className="criarperguntas-logo" alt="logo" />

      <div className = "criarperguntas-cadastrar">    
        <button onClick={()=> setModal(true)}> CADASTRAR PERGUNTA </button>
        { modal ? (
          <Modal onClose={ ()=> setModal(false) }>
            <Pergunta id={id ? Number.parseInt(id, 10) : null} />
          </Modal>
        ): null}
      </div>
    
      <div className = "criarperguntas-tabela">
        <tr>
          <th width = "40">ID</th>
          <th width = "204">TEMA</th>
          <th width = "450">ENUNCIADO</th>
          <th width = "90">EDITAR</th>
          <th width = "70">APAGAR</th>
        </tr>
          {pergunta.map((perguntas) => (<TabelaPergunta perguntas={perguntas}/>))}   
      </div>

      <div>
        <button className = "criarperguntas-voltar" onClick={voltar}> Voltar </button>
      </div>
              
      </header> 
    </div>
  );

};

export default CriarPerguntas;