import React, { useEffect, useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import logo from '../../novologo.png';
import '../../css/TelaResultados.css';

import AlunoContext from '../../store/AlunoContext';

const TelaResultado = () => {
     
  const history = useHistory();
  const [pontos, setPontos] = useState([]);
  const { tokenAluno } = useContext(AlunoContext);
    
  useEffect(() => {
      
      axios.get(`https://localhost:44321/api/Resultados/`) 
      .then((response) => {
        for(var i = 0; i < response.data.length; i++){
          if(response.data[i].alunoId === tokenAluno.idaluno){
            setPontos({pontuacao: response.data[i].pontuacao});
          }
        }
      });
      
  }, [tokenAluno.idaluno]);

  return (
    <div className="resultados-app">
      <header className="resultados-header">
       
        <img src={logo} className="resultados-logo" alt="logo" />
        
        <h1> Fim de Jogo </h1>
        
        <br/> <br/> <span> Você conquistou {pontos.pontuacao} pontos nessa Partida !</span> <br/> <br/>

        <button onClick = {() => history.push('/Aluno')} className= "resultados-voltar"> Voltar </button>

      </header>
    </div>
  );
}

export default TelaResultado;