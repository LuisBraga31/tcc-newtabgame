import React from 'react';
import './Resultados.css'

const Resultados = ({ resultados }) => {
    
  return (
    
    <div className = "resultados-component">
      <table>
      <tbody>
        <tr>
        <td width = "21"> {resultados.id} </td>
        <td width = "180"> {resultados.nickname} </td>
        <td width = "182"> {resultados.qtdperguntas} </td>
        <td width = "180"> {resultados.pontuacao} </td>
        </tr>
      </tbody>
      </table>    
    </div>

  )
};

export default Resultados;