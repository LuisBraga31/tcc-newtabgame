import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import axios from 'axios';
import logo from '../../novologo.png';
import '../../css/TelaResultados.css';

import Resultados from './Components/Resultados'

const TelaResultadoProfessor = () => {
     
  const history = useHistory();
  const { id } = useParams();
  const [alunos, setAlunos] = useState([]);
  const [resultados, setResultados] = useState([]);
  const [partida, setPartida] = useState([]);
  const [jogo, setJogo] = useState([]);
  const [jogosperguntas, setJogosPerguntas] = useState([]);
  const exibicao = useState([]);
  const [exibir, setExibir] = useState([]);

  useEffect(() => {     

    axios.get(`https://localhost:44321/api/Partidas/${id}`)
    .then((response) => {
      setPartida(response.data);
      console.log(response.data);
      axios.get(`https://localhost:44321/api/Jogos/${response.data.jogoId}`)
      .then((response) => {
        setJogo(response.data);
      });
    });

    axios.get(`https://localhost:44321/api/JogosPerguntas`)
    .then((response) => {
      setJogosPerguntas(response.data);
      console.log(response.data); 
    });

    axios.get(`https://localhost:44321/api/Jogadores`)
    .then((response) => {
      setAlunos(response.data); 
            console.log(response.data);
    });

    axios.get(`https://localhost:44321/api/Resultados`)
    .then((response) => {
      setResultados(response.data); 
            console.log(response.data);
    });


    }, [id]);

  function exibirJogadores(){
    
    for(var i = 0; i < alunos.length; i++){
        
      const a = partida.id;
      const b = alunos[i].partidaId;

      const busca = jogosperguntas => jogosperguntas.jogoId === jogo.id;
      const pesquisaPerguntaId = jogosperguntas.filter(busca).map(p => p.perguntaId);
      console.log(pesquisaPerguntaId);

      for(var j =0; j < resultados.length; j++){
        
        if(a === b){
        if(resultados[j].alunoId === alunos[i].id) {
            exibicao.push({id: alunos[i].id, nickname: alunos[i].nickname, qtdperguntas: pesquisaPerguntaId.length, pontuacao: resultados[j].pontuacao});
        }
        }

      }
      
    }

    exibicao.shift(); exibicao.shift();
    setExibir(exibicao); 

  }

  useEffect(() => {  
      exibirJogadores(); 
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="resultados-app">
      <header className="resultados-header">
       
        <img src={logo} className="resultados-logo" alt="logo" />
        <div className="resultados-exibir">
        <h3>Resultados dos Jogadores </h3>
        <button onClick={exibirJogadores}> Exibir Resultados </button>
        <div className= "resultados-tabela">
            <tr>
            <th width = "40">ID</th>
            <th width = "200">JOGADOR</th>
            <th width = "200">TOTAL PERGUNTAS</th>
            <th width = "200">PONTUAÇÃO FINAL</th>
            </tr>
        {exibir.map((resultados) => (<Resultados resultados={resultados} /> ))} 
      </div>
      </div>
      <button onClick = {() => history.push('/Professor')} className= "resultados-voltar"> Voltar </button>
      
      </header>
    </div>
  );
}

export default TelaResultadoProfessor;