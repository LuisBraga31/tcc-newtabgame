import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import './Cadastro.css';

const valores = {
    nome: '',
    email: '',
    nascimento: '',
    senha: '',
};

const Cadastro = () => {
  
    const [cadastros, setCadastros] = useState(valores);
    const history = useHistory();

    function onChange(ev) {
        const { name, value } = ev.target;
        setCadastros({ ...cadastros, [name]: value });
    }

    function cadastrando(ev){
        ev.preventDefault();
        if(cadastros.nome !== "" && cadastros.email !== "" && cadastros.nascimento !== "" && cadastros.senha !== "") {
        axios.post('https://localhost:44321/api/Professores', cadastros)
        .then((response) => {
            history.push('/Login');
        }); 
        } else {
            alert('Complete todos os campos!');
            return;
        }
    }

  return (
    <div>
    <div className = "cadastro-app">       
        <h1> PREENCHA OS CAMPOS </h1>
        <form onSubmit={cadastrando}>

            <input placeholder ="Nome" id = "nome" name = "nome" type = "text" onChange={onChange}></input>
            <input placeholder ="Email" id = "email" name = "email" type = "text" onChange={onChange}></input>
            <input placeholder ="Senha" id= "senha" name= "senha" type = "password" onChange={onChange}></input>
            <input placeholder ="Nascimento" id= "nascimento" name= "nascimento" type = "date" onChange={onChange} ></input>

            <button type = "submit"> Cadastrar </button>
        </form>

    </div>
    </div>
  );
}

export default Cadastro;