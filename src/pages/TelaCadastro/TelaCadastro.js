import React from 'react';
import { useHistory } from 'react-router-dom';
import logo from '../../novologo.png';
import '../../css/TelaCadastro.css';

import Cadastro from './Components/Cadastro';

const TelaCadastro = () => {
  
  const history = useHistory();

  function voltar(){
    history.push('/Login');
  }

  return (
    <div className = "telacadastro-app">       
      <header className="telacadastro-header">
        <img src={logo} className="telacadastro-logo" alt="logo" />
        
          <Cadastro/>
               
        <button className= "telacadastro-voltar" onClick={voltar}> Voltar </button>
      
      </header>
    </div>
  );
}

export default TelaCadastro;