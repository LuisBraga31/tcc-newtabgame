/* eslint-disable eqeqeq */
import React, { useEffect, useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import './Login.css';

import StoreContext from '../../../store/Context';

const Login = () => {
      
  const history = useHistory();
  const [entrada, setEntrada] = useState([]);
  const [professores, setProfessores] = useState([]);
  const { setToken } = useContext(StoreContext);

  useEffect(() => {
    axios.get(`https://localhost:44321/api/Professores`) 
    .then((response) => {
      setProfessores(response.data);
    })
  }, []); 

  function onChange(ev) {
    const { name, value } = ev.target;
    setEntrada({ ...entrada, [name]: value });
    console.log(entrada.email);
  }

  function login(ev){
    ev.preventDefault();
    var a = 0;
    
    for(var p = 0; p < professores.length; p++){
      if(professores[p].email == entrada.email && professores[p].senha == entrada.senha){
        a = 1;
        setToken({professor: professores[p].nome, identificador: professores[p].id});
        return history.push("/professor");
      }
    }

    if(a === 0){
      alert("Email ou Senha inválidos!");
      return;
    }

  }

  function cadastrar(ev){
    ev.preventDefault();
    history.push("/cadastro");    
  }

  return (
    <div>
    <div className = "login-app">
      <h1> ACESSAR O SISTEMA </h1>
       
      <input placeholder = "Digite seu Email" id = "email" name = "email" type ="text" onChange={onChange}/>
      <input placeholder = "Digite sua Senha" id = "senha" name = "senha" type ="password" onChange={onChange}/>

      <button onClick={login} disabled={!entrada.email || !entrada.senha} > Entrar </button>
      <button onClick={cadastrar}> Cadastrar </button>       

    </div>
    </div>

  );
}

export default Login;