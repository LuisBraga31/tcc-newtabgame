import React from 'react';
import { useHistory } from 'react-router-dom';

import logo from '../../novologo.png';
import Login from './Components/Login';
import '../../css/TelaLogin.css';

const TelaLogin = () => {
  
  const history = useHistory();

  function voltar(){
    history.push('/');
  }

  return (
    <div className = "telalogin-app">       
      <header className="telalogin-header">
      
      <img src={logo} className="telalogin-logo" alt="logo" /> 
            
        <Login/>

      <button className= "telalogin-voltar" onClick={voltar}> Voltar </button>
      
      </header>
    </div>
  );
}

export default TelaLogin;