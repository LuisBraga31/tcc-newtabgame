import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import axios from 'axios';
import logo from '../../novologo.png';
import '../../css/IniciandoPartida.css';

import EscolherJogo from './Components/EscolherJogo';

const IniciandoPartida = () => {
  
  const { id } = useParams();
  const history = useHistory();
  const [jogo, setJogo] = useState([]);
  const [title, setTitle] = useState([]);

  useEffect(() => {  
    const params = {};

    axios.get('https://localhost:44321/api/Jogos', {params })
    .then((response) => {
      setJogo(response.data);
    });

    axios.get(`https://localhost:44321/api/Partidas/${id}`)
    .then((response) => {
      setTitle(response.data);
    });

    }, [id]);

  function cancelar(ev){
    ev.preventDefault();
      
    axios.delete(`https://localhost:44321/api/Partidas/${title.id}`)
      .then((response) => {
        history.push('/partida');
      });
  }
      
  return (
    <div className="iniciando-app">
      <header className="iniciando-header">
      <img src={logo} className="iniciando-logo" alt="logo" />
        
        <label>Código - {title.codigo}</label>
        
        <div className = "iniciando-tabela">
          <tr>
          <th width = "40">#</th>
          <th width = "528">JOGO DISPONÍVEL</th>
          <th width = "95">ESCOLHER</th>
          </tr>
          {jogo.map((jogos) => (<EscolherJogo jogos={jogos} id={id ? Number.parseInt(id, 10) : null} /> ))}
        </div>

        <div>
          <button className= "iniciando-voltar" type = "button" onClick={cancelar}> Cancelar </button>
        </div>
      </header>
    </div>
  );
}

export default IniciandoPartida;