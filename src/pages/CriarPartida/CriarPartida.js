import React from 'react';
import { useHistory } from 'react-router-dom';
import logo from '../../novologo.png';
import '../../css/CriarPartida.css';

import Partida from './Components/Partida';

const CriarPartida = () => {
  
  const history = useHistory();

  return (
    <div className="criarpartida-app">
      <header className="criarpartida-header">
      <img src={logo} className="criarpartida-logo" alt="logo" />
        
        <h1> NOVA PARTIDA </h1>

        <Partida/>
        
        <div>
          <button className= "criarpartida-voltar" onClick={() => history.push('/Professor')}> Voltar </button>
        </div>

      </header>
    </div>
  );
}

export default CriarPartida;