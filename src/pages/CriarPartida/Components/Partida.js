import React from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import './Partida.css';
    
const cod = Math.random().toString(36).substr(2, 5);
    
const Partida = () => {

    const history = useHistory();

    function dois(ev) {
        ev.preventDefault();
        
        const valores2 = {
            codigo: cod,
            qtdJogadores: "2"
        };

        axios.post('https://localhost:44321/api/Partidas', valores2)
        .then((response) => {
            history.push(`/IniciandoPartida/${response.data.id}`);
        });
    }
    function tres(ev) {
        ev.preventDefault();
                
        const valores3 = {
            codigo: cod,
            qtdJogadores: "3"
        };
        
        axios.post('https://localhost:44321/api/Partidas', valores3)
        .then((response) => {
            history.push(`/IniciandoPartida/${response.data.id}`);
        });
    }
    function quatro(ev) {
        ev.preventDefault();
                
        const valores4 = {
            codigo: cod,
            qtdJogadores: "4"
        };
        
        axios.post('https://localhost:44321/api/Partidas', valores4)
        .then((response) => {
            history.push(`/IniciandoPartida/${response.data.id}`);
        });
    }
    
    return (     
        
        <div className="partida">
            <label> QUANTIDADE DE JOGADORES </label>
            <button onClick={dois}> 2 </button>
            <button onClick={tres}> 3 </button>
            <button onClick={quatro}> 4 </button>
        </div>
    )
};

export default Partida;