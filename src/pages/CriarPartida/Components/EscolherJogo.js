import React, { useState, useEffect, useContext } from 'react';
import StoreContext from '../../../store/Context';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import './EscolherJogo.css';

 const EscolherJogo = ({ jogos, id }) => {
        
    const [partida, setPartida] = useState([]);
    const history = useHistory();
    const { token } = useContext(StoreContext);

    useEffect(() => {

      axios.get(`https://localhost:44321/api/Partidas/${id}`) 
      .then((response) => {
        setPartida(response.data);
        console.log(response.data);
      })

    }, [id]); 

    function onSubmit(ev){
      ev.preventDefault();
      
      var inicioPartida = new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString();
      
      const jogoEscolhido = {
        id: partida.id,
        codigo: partida.codigo,
        state: false,
        rodada: -1,
        qtdJogadores: partida.qtdJogadores,
        partidaInicio: inicioPartida,
        partidaFim: partida.partidaFim,
        jogoId: jogos.id,
        professorId: token.identificador
      };
        axios.put(`https://localhost:44321/api/Partidas/${id}`, jogoEscolhido)      
        .then((response) => {
          history.push(`/Sala/${partida.id}`);
        });

    }       

    return (
    
      <div className = "escolha">
      <form onSubmit={onSubmit}>
        <table>
        <tbody>
          <tr> 
          <td width = "21">{jogos.id}</td>
          <td width = "509">{jogos.titulo}</td>
          <td width = "55"><button type = "submit"> Escolher </button></td>
          </tr>
        </tbody>
        </table> 
      </form>
      </div>
      
    )

 };

export default EscolherJogo;