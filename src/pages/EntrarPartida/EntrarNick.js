import React from 'react';
import { useHistory } from 'react-router-dom';
import logo from '../../novologo.png';
import '../../css/EntrarNick.css';

import EntrandoNick from './Components/EntrandoNick';

const EntrarNick = () => {

  const history = useHistory();

  function avançar(){
    history.push('/Aluno');
  }

  return (
    <div className = "entrarnick-app">
      <header className="entrarnick-header">
      
      <img src={logo} className="entrarnick-logo" alt="logo" />
        
        <EntrandoNick/>

      <button onClick={avançar} className= "entrarnick-voltar"> Voltar </button>

      </header>
    </div>
  );
}

export default EntrarNick;