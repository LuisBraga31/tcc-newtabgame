/* eslint-disable eqeqeq */
import React, { useEffect, useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import './EntrandoPartida.css';

import AlunoContext from '../../../store/AlunoContext';

import 'react-notifications-component/dist/theme.css';
import 'animate.css';

const EntrandoPartida = () => {

    const history = useHistory();

    const [title, setTitle] = useState([]);
    const [entrarPartida, setEntrarPartida] = useState([]);
    const { tokenAluno } = useContext(AlunoContext);
    const { setTokenAluno } = useContext(AlunoContext);
    
    useEffect(() => {
      axios.get(`https://localhost:44321/api/Partidas`) 
      .then((response) => {
        setEntrarPartida(response.data);
      });

      axios.get(`https://localhost:44321/api/Jogadores`)
      .then((response) => {
      for(var i = 0; i < response.data.length; i++) {
        if(response.data[i].nickname == tokenAluno.aluno){
          setTokenAluno({aluno:tokenAluno.aluno, idaluno:response.data[i].id})
        }
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []); 

    function onChange(ev) {
      const { name, value } = ev.target;
      setTitle({ ...title, [name]: value });
    }

    function onSubmit(ev){
      ev.preventDefault();
      var a = 0;
      
      for(var e = 0; e < entrarPartida.length; e++){
        if(entrarPartida[e].codigo === title.codigo && entrarPartida[e].partidaInicio != "0001-01-01T00:00:00"){
        
        a = 1;
        const atualizaJogador = {
        id: tokenAluno.idaluno,
        nickname: tokenAluno.aluno,
        partidaId: entrarPartida[e].id
        }

        axios.put(`https://localhost:44321/api/Jogadores/${tokenAluno.idaluno}`, atualizaJogador)      
        .then((response) => {  
          
          const result = {
            AlunoId: tokenAluno.idaluno,
            Pontuacao: 0
          }
          axios.post('https://localhost:44321/api/Resultados', result);
          history.push(`/Sala/${atualizaJogador.partidaId}`);

        });
        }

      } 
      
      if(a === 0){
      alert("Jogo não existe ou não foi iniciado!");
      return;
      }
    
    }
      
  return (
    <div>
    <div className = "entrandopartida-app">
      <h1> ENTRANDO NA PARTIDA </h1>  
      <input placeholder = "Digite o Código" id = "codigo" name = "codigo" type ="text" onChange={onChange}></input>

      <button className= "entrandopartida-entrar" onClick={onSubmit} > Entrar </button>          
        
    </div>
    </div>
  );
}

export default EntrandoPartida;