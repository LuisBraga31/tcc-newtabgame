/* eslint-disable eqeqeq */
import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import './EntrandoPartida.css';
import 'react-notifications-component/dist/theme.css';
import 'animate.css';

import AlunoContext from '../../../store/AlunoContext';

const EntrandoNick = () => {

    const history = useHistory();

    const [nick, setNick] = useState([]);
    const { setTokenAluno } = useContext(AlunoContext);

    function onChange(ev) {
      const { name, value } = ev.target;
      setNick({ ...nick, [name]: value });
    }

    function onSubmit(ev){
        ev.preventDefault();
        setTokenAluno({aluno:nick.nickname, idaluno: 0});
        axios.post('https://localhost:44321/api/Jogadores', nick)
        .then((response) => {
          history.push('/entrar');
        });
    }
      
  return (
    <div>
    <div className = "entrandopartida-app">
      <h1> ENTRANDO NA PARTIDA </h1>
      <input placeholder = "Digite um Nickname" id = "nickname" name = "nickname" type ="text" onChange={onChange}></input>   

      <button className= "entrandopartida-entrar" onClick={onSubmit} > Avançar </button>          
    </div>
    </div>
  );
}

export default EntrandoNick;