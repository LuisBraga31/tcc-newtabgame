import React from 'react';
import { useHistory } from 'react-router-dom';
import logo from '../../novologo.png';
import '../../css/EntrarPartida.css';

import EntrandoPartida from './Components/EntrandoPartida';

const EntrarPartida = () => {

  const history = useHistory();

  function sair(){
    history.push('/Aluno');
  }

  return (
    <div className = "entrarpartida-app">
      <header className="entrarpartida-header">
      
      <img src={logo} className="entrarpartida-logo" alt="logo" />
        
        <EntrandoPartida/>

      <button onClick={sair} className= "entrarpartida-voltar"> Sair </button>

      </header>
    </div>
  );
}

export default EntrarPartida;