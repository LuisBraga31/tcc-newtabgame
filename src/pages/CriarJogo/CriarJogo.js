import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import logo from '../../novologo.png';
import '../../css/CriarJogo.css';

import Jogo from "./Components/Jogo";
import TabelaJogos from "./Components/TabelaJogos";

const CriandoJogo = () => {
  
  const history = useHistory();
  const [jogo, setJogo] = useState([]);

  useEffect(() => {  
    const params = {};

    axios.get('https://localhost:44321/api/Jogos', { params })
    .then((response) => {
      setJogo(response.data);
    });

  }, []);

  return (
    <div className="criarjogo-app">
      <header className="criarjogo-header">
      <img src={logo} className="criarjogo-logo" alt="logo" />

      <Jogo/>
        
      <div className= "criarjogo-tabela">
        <tr>
        <th width = "40">#</th>
        <th width = "500">JOGOS CRIADOS</th>
        <th width = "80">EDITAR</th>
        <th width = "86">APAGAR</th>
        </tr>
        {jogo.map((jogos) => (<TabelaJogos jogos={jogos} /> ))} 
      </div>

      <div>
        <button className= "criarjogo-voltar" onClick={ () => history.push('/Professor')}> Voltar </button>
      </div>

      </header>
    </div>
  );
}

export default CriandoJogo;