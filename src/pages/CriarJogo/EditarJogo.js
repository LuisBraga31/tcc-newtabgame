import React, { useEffect, useState } from 'react'
import { useParams, useHistory } from 'react-router-dom';
import axios from 'axios';
import logo from '../../novologo.png';
import '../../css/EditarJogo.css';

import AdicionarPergunta from './Components/AdicionarPergunta';
import ListaModal from './Components/ListaModal.js';
import TabelaJogoPergunta from './Components/TabelaJogoPergunta.js';

import Modal from '../CriarPerguntas/Components/Modal.js';
import Pergunta from '../CriarPerguntas/Components/Pergunta';

const EditarJogo = () => {

  const { id } = useParams();
  const history = useHistory();
  const [modal, setModal] = useState(false);
  const [modalLista, setmodalLista] = useState(false);

  const [pergunta, setPerguntas] = useState([]);
  const [title, setTitle] = useState([]);
  const [jogosperguntas, setJogosPerguntas] = useState([]);
  
  const [testador, setTestador] = useState([]);
  const uniao = useState([]);

  useEffect(() => {     

    axios.get(`https://localhost:44321/api/Jogos/${id}`)
    .then((response) => {
      setTitle(response.data);
    });

    axios.get(`https://localhost:44321/api/JogosPerguntas`)
    .then((response) => {
      setJogosPerguntas(response.data); 
    });

    axios.get(`https://localhost:44321/api/Perguntas/`)
    .then((response) => {
      setPerguntas(response.data);  
    });

    }, [id]);

  function unir() {
    const busca = jogosperguntas => jogosperguntas.jogoId === title.id;
    const listaPerguntas = jogosperguntas.filter(busca).map(p => p.perguntaId);

    for(var i = 0; i < pergunta.length; i++){
      
      for (var p = 0; p < listaPerguntas.length; p++) {
        
        const a = pergunta[i].id;
        const b = listaPerguntas[p];
        console.log(a);
        console.log(b);
        if(a === b) {
          uniao.push(pergunta[i]);
        }

      }


    }
    uniao.shift();
    uniao.shift();
    console.log(uniao);
    setTestador(uniao); 
    
  }

  function onChange(ev) {
    const { name, value } = ev.target;
    setTitle({ ...title, [name]: value });
  }
    
  function onSubmit(ev){
    ev.preventDefault();
    axios.put(`https://localhost:44321/api/Jogos/${id}`, title)
    .then((response) => {
      history.push('/jogo');
    });
  }

  function voltar(){
    history.push('/jogo');
  }

  return (    
    <div onMouseMove={unir} className = "editar-app">
      <header className = "editar-header">
      <img src={logo} className = "editar-logo" alt="logo"/>
        
        <input id = "titulo" name = "titulo" type = "text"  onChange={onChange} value={title.titulo} ></input>
        
        <div className = "editar-modal">       
          
          <button className = "editar-botao" onClick={()=> setmodalLista(true)}> SELECIONAR PERGUNTAS </button>
          {modalLista ? (
            <ListaModal onClose={ ()=> setmodalLista(false) }>   
            <div className="editar-tabela">
              <tr>
              <th width = "30">#</th>
              <th width = "168">TEMA</th>
              <th width = "390">ENUNCIADO</th>
              <th width = "70">SELECIONAR</th>
              </tr>
            {pergunta.map((perguntas) => (
                <AdicionarPergunta perguntas={perguntas} id={id ? Number.parseInt(id, 10) : null} /> ))}
            </div> 
            </ListaModal>
          ): null}
            
          <button className = "editar-botao" onClick={()=> setModal(true)}> CADASTRAR PERGUNTA </button>
          {modal ? (
            <Modal onClose={ ()=> setModal(false) }>
                <Pergunta />
            </Modal>
          ): null}

        </div>
            
        <div className = "editar-tabela">
          <tr>
          <th width = "35">#</th>
          <th width = "204">TEMA</th>
          <th width = "370">ENUNCIADO</th>
          </tr>
          <div className="tabela-jogosperguntas">
          {testador.map((uniao) => (<TabelaJogoPergunta perguntas={uniao} /> ))} 
          </div>
        </div>
        
        <div>
          <button onClick={onSubmit} className = "editar-salvar" type="submit"> Salvar Alterações </button>
        </div>
        
        <div>
          <button className = "editar-voltar" onClick={voltar}> Voltar </button>
        </div>
        
      </header>
    </div>
  );
}

export default EditarJogo;