import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './AdicionarPergunta.css';

 const AdicionarPergunta = ({ perguntas, id }) => {
        
  const [valores, setValores] = useState([]);

  useEffect(() => {
      axios.get(`https://localhost:44321/api/Jogos/${id}`)
      .then((response) => {
        setValores(response.data);
      })    
  }, [id]); 

  function onSubmit(ev){
      ev.preventDefault();
    
      const infos = {
        jogoId: valores.id,
        perguntaId: perguntas.id
      };

      axios.post('https://localhost:44321/api/JogosPerguntas', infos)
      .then((response) => {
        alert('Pergunta Adicionada');
        return;
      });

  }       

  return (

      <div>
      <form onSubmit={onSubmit}>
        <div className = "tabela-adicionarPergunta">
        <table>
        <tbody>
          <tr>
          <td width = "10">{perguntas.id}</td>
          <td width = "150">{perguntas.tema}</td>
          <td width = "370">{perguntas.enunciado}</td>
          <td width = "80"><button type = "submit">Adicionar</button></td>          
          </tr>
        </tbody>
        </table>
        </div>
      </form>
      </div>
  )

 };

export default AdicionarPergunta;