import React from 'react';
import { useHistory } from 'react-router-dom';
import { BiTrash } from 'react-icons/bi';
import axios from 'axios';
import './TabelaJogos.css';
  
const TabelaJogos = ({ jogos }) => {
  
  const history = useHistory();

  function editar(){
    history.push(`/EditarJogo/${jogos.id}`);
  }

  function deletar(ev){
    ev.preventDefault();

    axios.delete(`https://localhost:44321/api/Jogos/${jogos.id}`)
    .then((response) => {
      history.push('/CriandoJogo');
    });
  }

  return (
    
    <div className = "tabela-jogos">
      <table>
      <tbody>
        <tr>
        <td width = "20">{jogos.id}</td>
        <td width = "482">{jogos.titulo}</td>
        <td><button width = "65" onClick={editar}> Editar </button> </td>
        <td width = "67"> <button type = "button" onClick={deletar}> <BiTrash/> </button> </td>
        </tr>
      </tbody>
      </table>    
    </div>

  )
};

export default TabelaJogos;