import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import './Jogo.css';

const valor = {
    titulo: ''
}    

const Jogo = () => {

   const [title, setTitle] = useState(valor);
   const history = useHistory();
   
    function onChange(ev) {
      const { name, value } = ev.target;
      setTitle({ ...title, [name]: value });
    }

    function onSubmit(ev) { 
        ev.preventDefault();

        axios.post('https://localhost:44321/api/Jogos', title)
        .then((response) => {
            history.push(`/EditarJogo/${response.data.id}`);
        });
    }
   
    return (

        <div className = "jogo">
            <form onSubmit={onSubmit}>
            <input placeholder = "Título" id = "titulo" name = "titulo" type = "text" onChange={onChange}></input>
            <button type = "submit"> CRIAR JOGO </button>
            </form>
        </div>
    )
};

export default Jogo;