import React from 'react';
import './ListaModa.scss';

const ListaModal =({ onClose = () => {}, children}) => {
    
return (
    
    <div className = "listaModal">      
        <div className = "listaModalContainer">
            <button className = "listaModalClose" onClick={onClose}/>
            <div className = "listaModalContent">{children}</div>
        </div>
    </div>
    
    )
};

export default ListaModal;