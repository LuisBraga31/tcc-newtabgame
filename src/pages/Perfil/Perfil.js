import React, { useContext, useEffect, useState } from 'react';
import StoreContext from '../../store/Context';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import logo from '../../novologo.png';
import '../../css/Perfil.css';

const Perfil = () => {
  
  const history = useHistory();
  const { token } = useContext(StoreContext);
  const [professor, setProfessor] = useState([]);

  useEffect(() => {
      axios.get(`https://localhost:44321/api/Professores/${token.identificador}`) 
      .then((response) => {
        setProfessor(response.data);
        console.log(response.data);
      })
  }, [token]); 

  function editar(){
    history.push('/editarperfil');
  }
  function voltar(){
    history.push('/Professor');
  }

  return (
    <div className = "perfil-app">
      <header className="perfil-header">
      <img src={logo} className="perfil-logo" alt="logo" />
        
        <div className="perfil">
        <h1> PERFIL DO PROFESSOR </h1>
        <div className = "dados">
          <label> Nome: {professor.nome} </label>
          <label> Email: {professor.email} </label>
          <label> Nascimento: {professor.nascimento} </label>
          <label> Senha: ***** </label>
          <button className= "perfil-editar" onClick={editar}> Editar Informações </button> <br/>
        </div>
        </div>

        <div>
          <button className= "perfil-voltar" onClick={voltar}> Voltar </button>
        </div>

      </header>
    </div>
  );
}

export default Perfil;