import React, { useContext, useEffect, useState } from 'react';
import StoreContext from '../../store/Context';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import logo from '../../novologo.png';
import '../../css/EditarPerfil.css';

const EditarPerfil = () => {
  
  const history = useHistory();
  const { token } = useContext(StoreContext);
  const [professor, setProfessor] = useState([]);

    useEffect(() => {
      axios.get(`https://localhost:44321/api/Professores/${token.identificador}`) 
      .then((response) => {
        setProfessor(response.data);
      })
    }, [token]);

    function voltar(){
      history.push('/perfil');
    }

    function onChange(ev) {
      const { name, value } = ev.target;
      setProfessor({ ...professor, [name]: value });
    }

    function alterar(ev) {
      ev.preventDefault();
      
      const editar = {
        id: professor.id,
        nome: professor.nome,
        email: professor.email,
        nascimento: professor.nascimento,
        senha: professor.senha,
      }

      axios.put(`https://localhost:44321/api/Professores/${token.identificador}`, editar)
      .then((response) => {
        history.push('/perfil');
        alert('Informações alteradas com sucesso!');
        return;
      }); 
    
    }

    return (
    <div className = "editarperfil-app">
      <header className="editarperfil-header">
      <img src={logo} className="editarperfil-logo" alt="logo" />
        
        <div className="editarperfil">
        <h1> PERFIL DO PROFESSOR </h1>
        <div className="dados-editar"> 
        <form onSubmit={alterar}>
          <input id= "nome" name = "nome" type = "text" onChange={onChange} value={professor.nome}></input>
          <input id= "email" name = "email" type = "text" onChange={onChange} value={professor.email}></input> 
          <input id= "nascimento" name = "nascimento" type = "text" onChange={onChange} value={professor.nascimento}></input> 
          <input id= "senha" name = "senha" type = "password" onChange={onChange} value={professor.senha}></input>
          <button type="submit" > Salvar Informações </button>
        </form>
        </div>
        </div>

        <div>
          <button className= "editarperfil-voltar" onClick={voltar}> Voltar </button>
        </div>

      </header>
    </div>
  );
}

export default EditarPerfil;