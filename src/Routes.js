import React, { useContext } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import StoreContext from './store/Context';
import AlunoContext from './store/AlunoContext';
import StoreProvider from './store/Provider';
import AlunoProvider from './store/AlunoProvider';

import Perfil from './pages/Perfil/Perfil';
import EditarPerfil from './pages/Perfil/EditarPerfil';
import TelaInicial from './pages/TelaInicial/TelaInicial';
import TelaInicialProfessor from './pages/TelaInicial/TelaInicialProfessor';
import TelaInicialAluno from './pages/TelaInicial/TelaInicialAluno';
import TelaLogin from './pages/TelaLogin/TelaLogin';
import TelaCadastro from './pages/TelaCadastro/TelaCadastro';
import TelaResultado from './pages/TelaResultados/TelaResultado';
import CriarPerguntas from './pages/CriarPerguntas/CriarPerguntas';
import CriarPartida from './pages/CriarPartida/CriarPartida';
import IniciandoPartida from './pages/CriarPartida/IniciandoPartida';
import CriarJogo from './pages/CriarJogo/CriarJogo';
import EditarJogo from './pages/CriarJogo/EditarJogo';
import EntrarPartida from './pages/EntrarPartida/EntrarPartida';
import ComoJogarProfessor from './pages/ComoJogar/ComoJogarProfessor';
import ComoJogarAluno from './pages/ComoJogar/ComoJogarAluno';
import Sala from './pages/TelaJogo/Sala/Sala';
import EntrarNick from './pages/EntrarPartida/EntrarNick';
import TelaResultadoProfessor from './pages/TelaResultados/TelaResultadosProfessor';

const RoutesPrivate = ({ component: Component, ...rest}) => {
  const { token } = useContext(StoreContext);
  return (
    <Route {...rest}
      render={() => token
        ? <Component {...rest} />
        : <Redirect to="/login" />
      }
    />
  )
}

const RoutesPrivateAluno = ({ component: Component, ...rest}) => {
  const { tokenAluno } = useContext(AlunoContext);
  return (
    <Route {...rest}
      render={() => tokenAluno
        ? <Component {...rest} />
        : <Redirect to="/Aluno" />
      }
    />
  )
}

const Routes = () => {
    return(
        <BrowserRouter>
            <StoreProvider>
            <AlunoProvider>
            <Switch>
                <Route path="/" exact component={TelaInicial} />

                <Route path="/login" exact component={TelaLogin}/>
                <Route path="/cadastro" exact component={TelaCadastro}/>

                <RoutesPrivate path="/professor" component={TelaInicialProfessor}/>
                <Route path="/aluno" exact component={TelaInicialAluno}/>

                <RoutesPrivate path="/perfil" component={Perfil} />
                <RoutesPrivate path="/editarperfil" component={EditarPerfil}/>
                <RoutesPrivate path="/perguntas" component={CriarPerguntas} />
                <RoutesPrivate path="/partida" component={CriarPartida} />
                <RoutesPrivate path="/jogo" component={CriarJogo}/>
                <RoutesPrivate path="/jogoProfessor" component={ComoJogarProfessor}/>
                <RoutesPrivate path="/resultadosPartida/:id" component={TelaResultadoProfessor}/>

                <Route path="/nick" component={EntrarNick}/>
                <Route path="/entrar" component={EntrarPartida}/>
                <Route path="/jogoAluno" component={ComoJogarAluno}/>
                <RoutesPrivateAluno path="/resultados" component={TelaResultado}/>

                <RoutesPrivate path="/edit/:id" component={CriarPerguntas}/>
                <RoutesPrivate path="/EditarJogo/:id" component={EditarJogo}/>
                <RoutesPrivate path="/IniciandoPartida/:id" component={IniciandoPartida}/>

                <Route path="/Sala/:id" component={Sala}/>
            
            </Switch>
            </AlunoProvider>
            </StoreProvider>
        </BrowserRouter>
    );
};

export default Routes;