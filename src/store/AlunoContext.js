import { createContext } from 'react';

const AlunoContext = createContext({
  tokenAluno:{
    aluno:"",
    idaluno:"",
  },
  setTokenAluno: () => {},
});

export default AlunoContext;