import React from 'react';
import AlunoContext from './AlunoContext';
import useStorage from '../utils/useStorage';

const AlunoProvider = ({ children }) => {
  
  const [tokenAluno, setTokenAluno] = useStorage('tokenAluno');

  return (
    <AlunoContext.Provider value={{tokenAluno, setTokenAluno,}}>
      {children}
    </AlunoContext.Provider>
  )
}

export default AlunoProvider;
