import { createContext } from 'react';

const StoreContext = createContext({
  token:{
    professor:"",
    identificador:"",
  },
  setToken: () => {},
});

export default StoreContext;